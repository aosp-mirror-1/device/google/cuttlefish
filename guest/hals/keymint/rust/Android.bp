//
// Copyright (C) 2022 The Android Open-Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

rust_binary {
    name: "android.hardware.security.keymint-service.rust",
    relative_install_path: "hw",
    vendor: true,
    srcs: ["src/keymint_hal_main.rs"],
    rustlibs: [
        "libandroid_logger",
        "libbinder_rs",
        "libhex",
        "libkmr_hal",
        "libkmr_hal_nonsecure",
        "libkmr_wire",
        "liblibc",
        "liblog_rust",
    ],
    prefer_rlib: true,
}

// init_rc
prebuilt_etc {
    name: "android.hardware.security.keymint-service.rust.rc",
    vendor: true,
    src: "android.hardware.security.keymint-service.rust.rc",
}

// vintf_fragments
prebuilt_etc {
    name: "android.hardware.security.keymint-service.rust.xml",
    sub_dir: "vintf",
    vendor: true,
    src: "android.hardware.security.keymint-service.rust.xml",
}

prebuilt_etc {
    name: "android.hardware.security.sharedsecret-service.rust.xml",
    sub_dir: "vintf",
    vendor: true,
    src: "android.hardware.security.sharedsecret-service.rust.xml",
}

prebuilt_etc {
    name: "android.hardware.security.secureclock-service.rust.xml",
    sub_dir: "vintf",
    vendor: true,
    src: "android.hardware.security.secureclock-service.rust.xml",
}

prebuilt_etc {
    name: "android.hardware.security.keymint-service.trusty.system.xml",
    sub_dir: "vintf",
    vendor: true,
    src: "android.hardware.security.keymint-service.trusty.system.xml",
}

prebuilt_etc {
    name: "android.hardware.security.sharedsecret-service.trusty.system.xml",
    sub_dir: "vintf",
    vendor: true,
    src: "android.hardware.security.sharedsecret-service.trusty.system.xml",
}

prebuilt_etc {
    name: "android.hardware.security.secureclock-service.trusty.system.xml",
    sub_dir: "vintf",
    vendor: true,
    src: "android.hardware.security.secureclock-service.trusty.system.xml",
}

// permissions
prebuilt_etc {
    name: "android.hardware.hardware_keystore.rust-keymint.xml",
    sub_dir: "permissions",
    vendor: true,
    src: "android.hardware.hardware_keystore.rust-keymint.xml",
}

apex_defaults {
    name: "com.android.hardware.keymint.rust_defaults",
    manifest: "manifest.json",
    key: "com.google.cf.apex.key",
    certificate: ":com.google.cf.apex.certificate",
    soc_specific: true,
    updatable: false,
    prebuilts: [
        // permissions
        "android.hardware.hardware_keystore.rust-keymint.xml",
    ],
}

apex {
    name: "com.android.hardware.keymint.rust_cf_remote",
    defaults: ["com.android.hardware.keymint.rust_defaults"],
    file_contexts: "file_contexts",
    binaries: [
        "android.hardware.security.keymint-service.rust",
    ],
    prebuilts: [
        // init_rc
        "android.hardware.security.keymint-service.rust.rc",
        // vintf_fragments
        "android.hardware.security.keymint-service.rust.xml",
        "android.hardware.security.secureclock-service.rust.xml",
        "android.hardware.security.sharedsecret-service.rust.xml",
    ],
}

apex {
    name: "com.android.hardware.keymint.rust_cf_guest_trusty_nonsecure",
    defaults: ["com.android.hardware.keymint.rust_defaults"],
    file_contexts: "file_contexts_trusty",
    prebuilts: [
        // vintf_fragments
        "android.hardware.security.keymint-service.trusty.system.xml",
        "android.hardware.security.secureclock-service.trusty.system.xml",
        "android.hardware.security.sharedsecret-service.trusty.system.xml",
    ],
}
