// Copyright (C) 2024 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

apex_key {
    name: "com.android.hardware.automotive.vehicle.test.key",
    public_key: "com.android.hardware.automotive.vehicle.test.avbpubkey",
    private_key: "com.android.hardware.automotive.vehicle.test.pem",
}

prebuilt_etc {
    name: "com.android.hardware.automotive.vehicle.cf.rc",
    src: "com.android.hardware.automotive.vehicle.cf.rc",
    installable: false,
}

apex {
    name: "com.android.hardware.automotive.vehicle.cf",
    manifest: "apex_manifest.json",
    key: "com.android.hardware.automotive.vehicle.test.key",
    file_contexts: "file_contexts",
    updatable: false,
    soc_specific: true,
    binaries: [
        "android.hardware.automotive.vehicle@V3-cf-service",
    ],
    prebuilts: [
        "com.android.hardware.automotive.vehicle.cf.rc",
    ],
    vintf_fragment_modules: ["android.hardware.automotive.vehicle@V3-cf-service.xml"],
}
